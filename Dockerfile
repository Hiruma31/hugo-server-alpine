FROM alpine:latest

ENV HUGO_VERSION=0.16

RUN apk add --update wget ca-certificates && \
  cd /tmp/ && \
  wget https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_linux-64bit.tgz && \
  tar xzf hugo_${HUGO_VERSION}_linux-64bit.tgz && \
  rm -r hugo_${HUGO_VERSION}_linux-64bit.tgz && \
  mv hugo /usr/bin/hugo && \
  apk del wget ca-certificates && \
  rm /var/cache/apk/*

WORKDIR /var/www

# Default values
ENV HUGO_BASE_URL=http://localhost
ENV HUGO_PORT=1313

CMD hugo server --appendPort=true --bind=0.0.0.0 --baseUrl=${HUGO_BASE_URL}:${HUGO_PORT}