hiruma/hugo-server-alpine
=========================

`hiruma/hugo-server-alpine` is a simple and lightweight image for serving static [Hugo](https://gohugo.io) websites based on [Alpine](https://hub.docker.com/_/alpine).


Prerequistes
------------

This image is a Hugo server only. You need an already generated hugo website with a structure looking like this:

  hugo_website
	| config.toml
	|-- archetypes/
	|-- content/
	|-- data/
	|-- layouts/
	|-- public/
	|-- static/
	|-- themes/

This folder will be mounted as a volume on /var/www.

Build the image
---------------

You can build the image simply by 
	docker build -t image/built .

Run the image
-------------

## docker run
The simpliest version is:
	docker run --rm -v /path/to/hugo/site:/var/www hiruma/hugo-server-alpine

You can extend it by using the environment variables:
	docker run --rm -v /path/to/hugo/site:/var/www -p XXXX:1313 \
	-e HUGO_PORT=XXXX \
	-e HUGO_BASE_URL=http://your_url \
	hiruma/hugo-server-alpine

## docker-compose

## Docker Cloud